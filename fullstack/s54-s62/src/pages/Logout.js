import { useContext, useEffect } from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {

	// localStorage.clear();

	const { unsetUser, setUser } = useContext(UserContext);

	// to clear the local storage upon logout
	unsetUser();

	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		});
	}, [setUser])

	return (
		// redirect the user back to login page
		<Navigate to="/login" />
	)
}