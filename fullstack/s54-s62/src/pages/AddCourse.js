import React, { useState, useContext } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function AddCourse() {
  const { user } = useContext(UserContext);

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  function addCourse(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/courses/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then(res => res.json())
      .then(data => {
        if (data) {
          setName("");
          setDescription("");
          setPrice("");

          Swal.fire({
            title: "Added Course",
            icon: "success",
            text: "You have successfully created a course.",
          });
        } else {
          Swal.fire({
            title: "Oops",
            icon: "error",
            text: "Something went wrong. Please try again.",
          });
        }
      })
      .catch(error => {
        console.error("Error adding course:", error);
      });
  }

  return (
    (user.id === null || user.isAdmin === false) ? 
        <Navigate to="/courses" />
     : 
        <Container>
          <Row>
            <Col>
              <Form onSubmit={(event) => addCourse(event)}>
                <h1 className="my-5 text-center">Add Course</h1>
                  <Form.Group controlId="courseName">
                      <Form.Label>Course Name:</Form.Label>
                      <Form.Control 
                          type="name" 
                          placeholder="Name"
                          value={name}
                          onChange={(event) => setName(event.target.value)}
                          required
                      />
                  </Form.Group>

                  <Form.Group controlId="description">
                      <Form.Label>Description</Form.Label>
                      <Form.Control 
                          type="description" 
                          placeholder="Description"
                          value={description}
                          onChange={(event) => setDescription(event.target.value)}
                          required
                      />
                  </Form.Group>

                  <Form.Group controlId="price">
                      <Form.Label>Price</Form.Label>
                      <Form.Control 
                          type="price" 
                          placeholder="Price"
                          value={price}
                          onChange={(event) => setPrice(event.target.value)}
                          required
                      />
                  </Form.Group>
                <Button
                  variant="primary"
                  type="submit"
                  id="submitBtn"
                >
                  Submit
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
  );
}