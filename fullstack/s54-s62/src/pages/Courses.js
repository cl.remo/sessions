// import coursesData from '../data/coursesData.js';
import { useEffect, useState, useContext } from 'react';
// import CourseCard from '../components/CourseCard.js';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

export default function Courses(){

	const {user} = useContext(UserContext);

	// State that will be used to store the courses retrieced from the database.
	const [courses, setCourses] = useState([]);

	// Retrieves the courses from the DB upon initial render of the "Courses" component
	// useEffect(() => {
	// 	fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data);

	// 		setCourses(data)
	// 	})
	// }, []);

	// Old approach
	// const courses = coursesData.map(course_item => {
	// 	return (
	// 		<CourseCard key={course_item.id} course={course_item}/>
	// 	)
	// })

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data)
		})
	};

	useEffect(() => {
		fetchData();
	}, []);


	console.log(user.isAdmin)

	return(
		<>
			{
				(user.isAdmin === true) ?
					<AdminView coursesData={courses} fetchData={fetchData} />
					:
					<UserView coursesData={courses} />
			}
		</>
	)
}