import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import {Navbar, Nav, Container} from 'react-bootstrap';
// import { BrowserRouter as Router } from 'react-router-dom';
// import { Route, Routes } from 'react-router-dom';

export default function Banner() {
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>Christian's Jack-of-all Trades</h1>
				<p>Looking for something? Got wares from A-Z.</p>
				<Link className="btn btn-primary" to="/login">Enroll Now!</Link>
			</Col>
		</Row>
	)
}
/*	const error = useRouteError();

	if (isRouteErrorResponse(error) && error.status === 404) {
		return (
			<Row>
				<Col className="p-5 text-center">
					<h1>404 - Not Found</h1>
					<p>The Page you are looking for cannot be found</p>
					<Nav.Link as={NavLink} to="/">Back Home</Nav.Link>
				</Col>
			</Row>
		)
	} */