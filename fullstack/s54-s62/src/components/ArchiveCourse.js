import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({course, isActive, fetchData}) {

	const archiveCourse = (courseId) => {

		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
			method : 'PUT',
			headers : {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title : 'Success!',
					icon : 'success',
					text : 'Course archived successfully.'
				})
				fetchData();
			} else {
				Swal.fire({
					title : 'Oh! Oh!',
					icon : 'error',
					text : 'Please try again.'
				})
				fetchData();
			}
		})
	}

	const activateCourse = (courseId) => {

		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/activate`, {
			method : 'PUT',
			headers : {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title : 'Success!',
					icon : 'success',
					text : 'Course activated successfully.'
				})
				fetchData();
			} else {
				Swal.fire({
					title : 'Oh! Oh!',
					icon : 'error',
					text : 'Please try again.'
				})
				fetchData();
			}
		})
	}

	return(
		<>
			{isActive ?
				<Button 
					variant="danger" 
					size="m"
					type="Submit"
					onClick={() => archiveCourse(course)}
				>
					Archive
				</Button>
			:
				<Button 
					variant="success" 
					size="m"
					type="Submit" 
					onClick={() => activateCourse(course)}
				>
					Activate
				</Button>
			}	
		</>
	)
}