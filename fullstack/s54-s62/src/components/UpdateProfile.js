// Generate a ReactJS component that will enable a user to change first name, last name and mobile number. Integrate it using Fetch API with the change user details feature. 
// - The backend change user details feature only requires a jwt token to be passed in the authorization headers
// - Use Bootstrap
// - Assume that the change user details feature is already done in the ExpressJS API.

import { useState } from 'react';
import Swal from 'sweetalert2';

const UserDetailsForm = ({ jwtToken }) => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');

  const handleUpdate = async () => {
    const updatedData = {
      firstName,
      lastName,
      mobileNo,
    };

    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(updatedData),
      });

      if (response.ok) {
        Swal.fire({
			title : 'Success!',
			icon : 'success',
			text : 'Profile updated successfully.'
		})
      } else {
        Swal.fire({
			title : 'Oh! oh!',
			icon : 'error',
			text : 'Something went wrong!'
		})
      }
    } catch (error) {
      Swal.fire({
			title : 'Oh! oh!',
			icon : 'error',
			text : 'An error occured!'
		})
    }
  };

  return (
    <div className="container mt-4">
      <h2>Change User Details</h2>
      <div className="mb-3">
        <label htmlFor="firstName" className="form-label">
          First Name
        </label>
        <input
          type="text"
          className="form-control"
          id="firstName"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="lastName" className="form-label">
          Last Name
        </label>
        <input
          type="text"
          className="form-control"
          id="lastName"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="mobileNumber" className="form-label">
          Mobile Number
        </label>
        <input
          type="text"
          className="form-control"
          id="mobileNumber"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
        />
      </div>
      <button className="btn btn-primary" onClick={handleUpdate}>
        Update
      </button>
    </div>
  );
};

export default UserDetailsForm;
