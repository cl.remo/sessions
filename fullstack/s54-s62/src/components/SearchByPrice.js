/*
Generate a reactjs component that will search for a course by price. Integrate it using Fetch API with the search by price feature. 
- The backend search course by price feature only requires the price to be passed in the request body.
- Use Bootstrap
- Assume that the search by price feature is already done in the ExpressJS API.
*/

import { useState } from 'react';

const SearchByPrice = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/courses/searchByPrice`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ minPrice, maxPrice }),
    });

    if (response.ok) {
      const data = await response.json();
      setSearchResults(data);
    } else {
      console.error('Error fetching data:', response.statusText);
    }
  };

  return (
    <div className="container mt-5">
      <h2>Course Search by Price Range</h2>
      <div className="mb-3">
        <label htmlFor="minPrice" className="form-label">Minimum Price:</label>
        <input
          type="number"
          className="form-control"
          id="minPrice"
          value={minPrice}
          onChange={(e) => setMinPrice(e.target.value)}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="maxPrice" className="form-label">Maximum Price:</label>
        <input
          type="number"
          className="form-control"
          id="maxPrice"
          value={maxPrice}
          onChange={(e) => setMaxPrice(e.target.value)}
        />
      </div>
      <button className="btn btn-primary" onClick={handleSearch}>Search</button>
      <div className="mt-4">
        <h4>Search Results:</h4>
        <ul>
          {searchResults.map(course => (
            <li key={course.id}>{course.name} - ${course.price}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default SearchByPrice;

