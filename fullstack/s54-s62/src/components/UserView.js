import { useState, useEffect } from 'react';
import CourseSearch from './CourseSearch.js';
import CourseCard from './CourseCard.js';
import SearchByPrice from './SearchByPrice.js';

export default function UserView({coursesData}) {

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		const coursesArr = coursesData.map(course => {
			if(course.isActive === true) {
				return (
					<CourseCard courseProp={course} key={course._id} />
				)
			} else {
				return null;
			}
		})

		setCourses(coursesArr);
	}, [coursesData])


	return (

		<>
			<h1 className="text-center">Courses</h1>
			<CourseSearch />
			<SearchByPrice />
			{ courses }
		</>
	)

}