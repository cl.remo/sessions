console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// The fetch()function returns a Promise which then can be chained using the then() function. Then then() function  waits for the promise to be resolved before executing code.
fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => response.json())
	.then(posts => console.log(posts));

// as of ES6
async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	let json_result = await result.json();

	console.log(json_result);
}

fetchData();


// Adding headers, body, and method to the fetch() function
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "New post!",
		body: "Hello World!",
		userId: 2
	})
})
.then(response => response.json())
.then(created_post => console.log(created_post));