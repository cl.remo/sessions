const express = require('express');
const router = express.Router();
const CourseController = require('../controllers/CourseController.js');
const auth = require('../auth.js');

// add single course
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.addCourse(request.body).then((result) =>{
		response.send(result);
	})
});

// get all course
router.get('/all', (request, response) => {
	CourseController.getAllCourses(request, response);
});

// get all active courses
router.get('/', (request, response) => {
	CourseController.getAllActiveCourses(request, response);
})

// get single course
router.get ('/:id', (request, response) => {
	CourseController.getCourse(request, response);
})

// 
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.updateCourse(request, response);
})

// activity 46
// member 1
router.put('/:id/archive', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.archiveCourse(request, response);
})

// member 3
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.activateCourse(request, response);
})

// Search course by name
router.post('/search', (request, response) => {
	CourseController.searchCourses(request, response);
});

module.exports = router;