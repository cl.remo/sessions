const Course = require('../models/Course.js');
const auth = require('../auth.js');

module.exports.addCourse = (request_body) => {
	let new_course = new Course({
		name : request_body.name,
		description : request_body.description,
		price : request_body.price
	});

	return new_course.save().then((added_course, error) => {
		if (error) {
			return {
				message: error.message
			}
		}
		return {
			message: 'Successfully added a course.'
		};
	}).catch(error => console.log(error));
}

module.exports.getAllCourses = (request, response) => {
	return Course.find({}).then(result => {
		return response.send(result);
	})
}

module.exports.getAllActiveCourses = (request, response) => {
	return Course.find({isActive: true}).then(result => {
		return response.send(result);
	})
}

module.exports.getCourse = (request, response) => {
	return Course.findById({_id: request.params.id}).then(result => {
		return response.send(result);
	})
}

module.exports.updateCourse = (request, response) => {
	let updated_course_details = {
		name : request.body.name,
		description : request.body.description,
		price : request.body.price
	};

	return Course.findByIdAndUpdate(request.params.id, updated_course_details).then((course, error) => {

		if (error) {
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Course has been updated successfully!'
		})
	})
}

// activity 46
// member 2
module.exports.archiveCourse = (request, response) => {
	let archive_course = {
		isActive: false
	}

	return Course.findByIdAndUpdate(request.params.id, archive_course).then((course, error) => {

		if (error) {
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Course has been archived!'
		})
	})
}

// member 4
module.exports.activateCourse = (request, response) => {
	let activate_course = {
		isActive: true
	}

	return Course.findByIdAndUpdate(request.params.id, activate_course).then((course, error) => {

		if (error) {
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Course has been activated!'
		})
	})
}

module.exports.searchCourses = (request, response) => {
      const courseName = request.body.courseName;

      return Course.find({ name: { $regex: courseName, $options: 'i' } }).then((courses) => {
      	response.send(courses)
      }).catch(error => response.send({
      	message: error.message
      }))
}