const express = require('express');
const router = express.Router();
const TaskController = require('../controllers/TaskController.js');

// Insert routes here
// Creating new tasks
router.post('/', (request, response) => {
	TaskController.createTask(request.body).then(result => {
		response.send(result);
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then(result => {
		response.send(result);
	})
})

// Update task status to "completed"
router.put('/:id/complete', (request, response) => {
	TaskController.updateStatus(request.params.id).then(reult => {
		response.send(result)
	})
})

module.exports = router;

/*
const express = require('express');
const router = express.Router();
const TaskController = require('../controllers/TaskController.js');

// Insert routes here
// Creating a new task
router.post('/', (request, response) => {
	TaskController.createTask(request.body).then(result => {
		response.send(result); 
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then(result => {
		response.send(result);
	})
})

module.exports = router;
*/