// Function Declaration and Invocation

function printName(){
	console.log("My name is Jeff");
}

printName();


// FUNCTION EXPRESSION
let variable_function = function(){
	console.log("Hello from function expression")
}

variable_function();


// SCOPING
let global_variable = "Call me Mr. Worldwide";

console.log(global_variable);

function showNames(){
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);
}

showNames();


// NESTED FUNCTION
function parentFunction(){
	let name = "Jane";

	function childFunction(){
		let nested_name = "John";

		// console.log(name);
	}

	childFunction();
}

parentFunction();

// BEST PRACTICE FOR FUNCTION NAMING
function printWelcomeMessageForUser(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome sa page ko!");
}

printWelcomeMessageForUser();


// RETURN STATEMENT
function fullName(){
	return "Christian Lloyd Remo";
}

fullName();