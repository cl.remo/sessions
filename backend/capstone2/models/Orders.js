const mongoose = require('mongoose');

// Schema
const order_schema = new mongoose.Schema({
	userId : {
		type : String,
		required : ['User Id is required!']
	},
	products : [
		productId : {
			type : String,
			required : ['Product Id is required!']
		},

		quantity : {
			type : Number,
			required : ['How many did the user purchase?']
		}
	],

	purchasedOn : {
		type : Date, 
		default : new Date()
	},

	totalAmount : {
		type : Number
		$multiply : 
	}
})

module.exports = mongoose.model("Order", order_schema);