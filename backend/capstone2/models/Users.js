const mongoose = require('mongoose');

// Schema
const user_schema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "Email is required."]
	},
	password : {
		type : String,
		required : [true, "Password is required."]
	},
	isAdmin : {
        type : Boolean, 
        default : false
    },
    orders : {
    	type: String,
    	required : [true, "Order number is required."]
    }
})

module.exports = mongoose.model("User", user_schema);