const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');

// add single product
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.addProduct(request.body).then((result) =>{
		response.send(result);
	})
});

// get all product
router.get('/all', (request, response) => {
	ProductController.getAllProducts(request, response);
});

// get all active products
router.get('/', (request, response) => {
	ProductController.getAllActiveProducts(request, response);
})

// get single product
router.get ('/:id', (request, response) => {
	ProductController.getProduct(request, response);
})

// update product details
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response);
})

// archive product
router.put('/:id/archive', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.archiveProduct(request, response);
})

// activate archived product
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request, response);
})

// Search product by name
router.post('/search', (request, response) => {
	ProductController.searchProducts(request, response);
});

module.exports = router;