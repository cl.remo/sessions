const User = require('../models/Users.js');
const Product = require('../models/Products.js');
const Order = require('../models/Orders.js');
const auth = require('../auth.js');

module.exports.order = (request, response) => {

	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}
	let isUserUpdated = await User.findById(request.user.id).then(user => {

		let new_order = {
			productId : request.body.productId
		}

		user.orders.push(new_order);

		return user.save().then(updated_user => true).catch(error => error.message);
		
	})

	// Sends any error within 'isUserUpdated' as a response
	if(isUserUpdated !== true){
		return response.send({ message: isUserUpdated });
	}

	// [SECTION] Updating product collection
	let isProductUpdated = await Product.findById(request.body.productId).then(product => {
		let new_enrollee = {
			userId: request.user.id
		}

		product.enrollees.push(new_enrollee);

		return product.save().then(updated_product => true).catch(error => error.message)
	})


	if(isProductUpdated !== true){
		return response.send({ message: isProductUpdated });
	}

	// [SECTION] Once isUserUpdated AND isProductUpdated return true
	if(isUserUpdated && isProductUpdated){
		return response.send({ message: 'Ordered Successfully!' });
	}
}

module.exports.getOrders = (request, response) => {
	User.findById(request.user.id)
		.then(user => response.send(user.orders))
		.catch(error => response.send(error.message))	
}