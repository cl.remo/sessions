const Product = require('../models/Products.js');
const auth = require('../auth.js');

// register product
module.exports.addProduct = (request_body) => {
	let new_product = new Product({
		name : request_body.name,
		description : request_body.description,
		price : request_body.price
	});

	return new_product.save().then((added_product, error) => {
		if (error) {
			return {
				message: error.message
			}
		}
		return {
			message: 'Successfully added a product.'
		};
	}).catch(error => console.log(error));
}

// lists all products
module.exports.getAllProducts = (request, response) => {
	return Product.find({}).then(result => {
		return response.send(result);
	})
}

// lists all active products for purchase
module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		return response.send(result);
	})
}

// list specific product
module.exports.getProduct = (request, response) => {
	return Product.findById({_id: request.params.id}).then(result => {
		return response.send(result);
	})
}

// update product details
module.exports.updateProduct = (request, response) => {
	let updated_product_details = {
		name : request.body.name,
		description : request.body.description,
		price : request.body.price
	};

	return Product.findByIdAndUpdate(request.params.id, updated_product_details).then((product, error) => {

		if (error) {
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Product has been updated successfully!'
		})
	})
}

// archive product
module.exports.archiveProduct = (request, response) => {
	let archive_product = {
		isActive: false
	}

	return Product.findByIdAndUpdate(request.params.id, archive_product).then((product, error) => {

		if (error) {
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Product has been archived!'
		})
	})
}

// activates archived product
module.exports.activateProduct = (request, response) => {
	let activate_product = {
		isActive: true
	}

	return Product.findByIdAndUpdate(request.params.id, activate_product).then((product, error) => {

		if (error) {
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Product has been activated!'
		})
	})
}

module.exports.searchProduct = (request, response) => {
      const productName = request.body.productName;

      return Course.find({ name: { $regex: productName, $options: 'i' } }).then((courses) => {
      	response.send(courses)
      }).catch(error => response.send({
      	message: error.message
      }))
}