// Server Variables
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const port = 4000;
const app = express();
const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// Routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);

// Database connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-remo.u779arb.mongodb.net/b303-ecommerce-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true 
});

mongoose.connection.on('error', () => console.log("Can't connect to database."));
mongoose.connection.once('open', () => console.log("Connected to MongoDB!"));

// Since we're hosting this API in the cloud, the port to be used should be flexible hence, the use of the process.env.PORT which will take the port that the cloud server uses if the 'port' variable isn't available.
app.listen(process.env.PORT || port, () => {
	console.log(`E-commerce System API is now running at localhost: ${process.env.PORT || port}`);
});

module.exports = app;