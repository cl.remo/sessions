let http = require('http');

const port = 4000;

const app = http.createServer((req,res) => {

	if(req.url == '/items' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end('Data retrieved from the database');
	}
})

app.listen(port, () => console.log(`Server is running at local host: ${port}`));