// [SECTION] Objects - a data type that is used to represent real world objects.
//  	- create properties and methods/functionalities

//  creating object and initializers/object literals
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using initializers/object literals");
console.log(cellphone);
console.log(typeof cellphone);

// creating objects using a constructor function
function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// multiple instance of an object using the "new" keyword
// this method is called instantiation
let laptop = new Laptop('Lenovo', 2008)
console.log("Result from creating objects using a constructor function");
console.log(laptop);

let laptop2 = new Laptop('MacBook Air', 2020)
console.log("Result from creating objects using a constructor function");
console.log(laptop2);


// [SECTION] Accessing Object Properties

// using square bracket notation
console.log("Result from square bracket notation: " + laptop2['name']);

// using dot notation
console.log("Result from dot notation: " + laptop2.name);

// access array objects
let array = [laptop, laptop2];

console.log(array[0]['name']);
console.log(array[0].name);


// [SECTION] Adding/Deleting/Reassigning Object Properties

// empty object
let car = {};

// adding object properties
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation: ")
console.log(car);

// adding object properties using square braccket notation
car['manufacturing date'] = 2019;
console.log(car['manufacturing date']);
console.log(car['Manufacturing Date']);


// console.log(car.manufacturing date);

