// [SECTION] IF-ELSE STATEMENTS
let number = 1;

if (number > 1) {
	console.log("The number is greater than 1!");
} else if (number < 1) {
	console.log("The number is less than 1!");
} else {
	console.log("None of the conditions were true :(");
}


// FALSY VALUES
if (false) {
	console.log("Falsey");
}

if (0) {
	console.log("Falsey");
}

if (undefined) {
	console.log("Falsey");
}

// TRUTHY VALUES
if (true) {
	console.log("Truthy");
}

if (1) {
	console.log("Truthy");
}

if ([]) {
	console.log("Truthy");
}


// TERNANRY OPERATORS
let result = (1 < 10) ? true : false;

// this is the equivalent of the ternanry operaation above.
// if (1 < 10) {
// 	return true;
// } else {
// 	return false;
// }

console.log("We've returned from the ternary operator is " + result);

// if there are multiple lines within the if-else block, it's better to use regular if-else statements 
if (5 == 5) {
	let	greeting = "hello";
	console.log(greeting);
}


// [SECTION] SWITCH STATEMENTS
let day = prompt("What day of the week is it today?").toLowerCase();

switch(day){
	case 'monday':
		console.log("The day today is monday!");
		break;
	case 'tuesday':
		console.log("The day today is tuesday!");
		break;
	case 'wednesday':
		console.log("The day today is wednesday!");
		break;
	case 'thursday':
		console.log("The day today is thursday!");
		break;
	case 'friday':
		console.log("The day today is friday!");
		break;
	case 'saturday':
		console.log("The day today is saturday!");
		break;
	case 'sunday':
		console.log("The day today is sunday!");
		break;
	default:
		console.log("Please input a valid day naman pareeeeh");
		break;
}


// [SECTION] TRY/CATCH/FINALLY
function showIntensityAlert(windspeed){
	try	{
		alerat(determineTyphoonIntensity(windspeed));
	} catch(error) {
		console.log(error.message)
	} finally {
		alert("Intensity updates will show new alert!");
	}
}

showIntensityAlert(56);