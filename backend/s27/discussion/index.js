console.log("Mutator Methods");

// Iteration Methods
/*
	- loops through all the elements to perform repetitive tasks on the array
*/

// forEach() - loops through all the array
// map() - loops through the array and returns a new array
// filter() - returns a new array containing elements which meets the given condition

// every() - checks if all elements meet the given condition
// return true if all elements meet the given condition, however, false if it does not
let numbers = [1, 2, 3, 4, 5, 6];

let allValid = numbers.every(function(number) {

	return number > 3;
})

console.log("result of every () method: ");
console.log(allValid);

// some() - checks if at least one element meets the given condition
let someValid = numbers.some(function(number) {

	return number < 2;
})

console.log('result of some () method: ');
console.log(someValid);

// includes() method - methods can be "chained" using then one after another
let products = ['Mouse', 'keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product) {

	return product.toLowerCase().includes('a');
})

console.log(filteredProducts);

// reduce()
let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {

	console.warn('current iteration: ' + ++iteration);
	console.log('accumulator: ')
})