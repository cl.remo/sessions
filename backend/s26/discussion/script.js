// [SECTION] Arrays and Indexes
let grades = [98.5, 94.3, 89.2, 90.1];
let computer_brands = [
  "Acer",
  "Asus",
  "Lenovo",
  "Neo",
  "Redfox",
  "Gateway",
  "Toshiba",
  "Fujitsu",
  "Lenovo",
];
let mixed_array = [12, "Asus", null, undefined, {}];

// Alternative way to write arrays
let my_tasks = ["drink html", "eat javascript", "inhale css", "base sass"];

// Reassigning values
console.log("Array before reassignment");
console.log(my_tasks);

my_tasks[0] = "run hello world"; //To reassign a value in an array,
// just use its index number and use an assignment operator to replace
// the value of that index
console.log("Array after reassignment");
console.log(my_tasks);

// [SECTION] Reading from Arrays
console.log(computer_brands[1]);
console.log(grades[3]);

// Getting the Length od an Array
console.log(computer_brands.length);

// Accessing last element in an array
let index_of_last_element = computer_brands.length - 1;
console.log(computer_brands[index_of_last_element]);

// [SECTION] Array Methods
let fruits = ["Apple", "Orange", "Kiwi", "PassionFruit"];

// 'push' Method - adds a new item to the end of the array
console.log("Current array: ");
console.log(fruits);

fruits.push("Mango", "Cocomelon");

console.log("Updated array after push method: ");
console.log(fruits);

// 'pop' Method - removes the last item of an array
console.log("Current array: ");
console.log(fruits);

let removed_item = fruits.pop();

console.log("Updated array after pop method: ");
console.log(fruits);
console.log("Removed fruit: " + removed_item);

// 'unshift' Method - Adding items in the beginning of the array
console.log("Current array: ");
console.log(fruits);

fruits.unshift("Lime", "Star Apple");

console.log("Updated array after unshift method: ");
console.log(fruits);

// 'shift' Method - removes the first item of an array
console.log("Current array: ");
console.log(fruits);

let removedItem = fruits.shift();

console.log("Updated array after shift method: ");
console.log(fruits);
console.log("Removed fruit: " + removedItem);

// 'splice' Method - Can be simultaneously update multiple items in an array
// starting from a specified index.
console.log("Current array: ");
console.log(fruits);

// splice(startingIndex, numberOfItems, itemsToBeAdded )
fruits.splice(1, 2, "Lime", "Cherry");

console.log("Updated array after splice method: ");
console.log(fruits);

// 'sort' Method
console.log("Current array: ");
console.log(fruits);

fruits.sort(); // sorts the items in order
// fruits.reverse() // Sorts the items in reverse order

console.log("Updated array after sort method: ");
console.log(fruits);

// [Sub-SECTION] Non-Mutator Methods - Every method written above is called
// a 'Mutator Method'  because it modifies the value of the array one way or
// anoter. Non-Mutator Methods on the other hand, don't do the same thing,
// they instead execute specific functionalities that can be done with the
// existing array values.

// 'indexOf' method - gets the index of a specific item
let index_of_lenovo = computer_brands.indexOf("Lenovo");
console.log("The index of lenovo is: " + index_of_lenovo);

// 'lastIndexOf' method - gets the index of a specific item starting from
// the end of the array
let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log(
  "The index of lenovo starting from the end of the array is: " +
    index_of_lenovo_from_last_item
);

// 'slice' Method
let hobbies = ["Gaming", "Running", "Gaslighting", "Cycling", "Writing"];
console.log(hobbies);

// By putting '2' as the argument, we are starting the slicing process from
// the item with the index of 2
let sliced_array_from_hobbies = hobbies.slice(2);
console.log(sliced_array_from_hobbies);

// By putting two arguments instead of one, we are also specifying where
// the slice will end
let sliced_array_from_hobbies_B = hobbies.slice(2, 4);
console.log(sliced_array_from_hobbies_B);

// By using a negative number as the index, the count where it will start
// which will come from the end of the array instead of the beginning
let sliced_array_from_hobbies_C = hobbies.slice(-3);
console.log(sliced_array_from_hobbies_C);

// 'toString' Method
let string_array = hobbies.toString();
console.log(string_array);

// 'concat' Method
let greeting = ["hello", "world"];
let exclamation = ["!", "?"];

let concat_greeting = greeting.concat(exclamation);
console.log(concat_greeting);

// 'join' Method - this will convert the array to a string and insert
// a specified separator between them. The separator is defined as the
// argument of the join() function
console.log(hobbies.join(" - "));

// 'forEach' Method
hobbies.forEach(function (hobby) {
  console.log(hobby);
});

// 'map' Method - loops throughout the whole array and adds each item to
// a new array
let numbers_list = [1, 2, 3, 4, 5];

let numbers_map = numbers_list.map(function (number) {
  return number * 2;
});
console.log(numbers_map);

// 'filter' Method
let filtered_numbers = numbers_list.filter(function (number) {
  return number < 3;
});
console.log(filtered_numbers);

// [SECTION] Multi-dimensional Arrays
let chess_board = [
  ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
  ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
  ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
  ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
  ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
  ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
  ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
  ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

console.log(chess_board[1][4]);
