// // [SECTION] WHILE LOOP
// let count = 5;

// while (count !== 0) {
// 	console.log("Current value of the count: " + count);
// 	count--;
// }


// // [SECTION] Do-while loop
// let number = Number(prompt("Give me a number: "));

// do {
// 	console.log("Current value of number: " + number);

// 	number += 1;
// } while (number < 10)


// // [SECTION] For Loop
// for (let count = 0; count <= 20; count++) {
// 	console.log("Current for loop value: " + count);
// }


// let my_string = "earl";

// // To get the lenght of the string
// console.log(my_string.length);

// // To get the specific letter in a string
// console.log(my_string[2]);

// for (let index = 0; index < my_string.length; index++) {
// 	console.log(my_string[index]);
// }

// MINI ACTIVITY (20 mins.)
// 1. Loop through the 'my_name' variable which has a string with your name on it.
// 2. Display each letter in the console but EXCLUDE all the vowels fom it.

let my_name = "Christian Lloyd Remo";
let noVowels = my_name.replace(/[aeiou]/gi, "");

for (let index = 0; index < my_name.length; index++) {
	console.log(noVowels[index]);
}

let name_two = "rafael";

for (let index = 0; index < name_two.length; index++); {
	console.log(name_two[index]);

	if (name_two[index].toLowerCase() == "a") {
		console.log("Skipping...");
		continue;
	}

	if (name_two[index] == "e") {
		
	}
}