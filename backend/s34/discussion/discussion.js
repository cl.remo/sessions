// CRUD Operations
/*
	- CRUD means Create, Retrieve, Update, and Delete.
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [Section] Inserting documents (Create)

// Insert one document
/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
    - By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/


//  [SECTION] Inserting Documents (CREATE)
db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"contact": {
		"phone": "09999999999",
		"email": "janedoe@mail.com"
	},
	"courses": ["CSS", "JavaScript", "Python"],
	"department": "none",
});

// Inserting multiple documents at once
db.users.insertMany([
	{
		"firstName" : "John",
		"lastName" : "Doe"	
	},
	{
		"firstName" : "Joseph",
		"lastName" : "Doe"
	}
]);


// [SECTION] Retrieving Documnents (READ)
// Retrieving all the inserted users
db.users.find();

// retrieving specific users
db.users.find({"firstName": "John"});


// [SECTION] Updating existing Documents (UPDATE)
db.users.updateOne(
	{
		"_id": ObjectId("64c0dffc2ece36edcfa528f6")
	},
	{
		$set: {
			"lastName": "Remo"
		}
	}
);

// For updating multiple documents - updateMany() allows for modification of 2 or more documents as compared to 
db.users.updateMany(
	{
		"lastName": "Doe"
	},
	{
		$set: {
			"firstName": "Mary"
		}
	}
);


// [SECTION] Deleting Documents from a collection
// deleting for multiple documents
db.users.deleteMany({"lastName": "Doe"});

// Deleting a single document
db.users.deleteOne({
	"_id": ObjectId("64c0dffc2ece36edcfa528f6")
});