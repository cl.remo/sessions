// Greater than operator
db.user.find({
	age: {
		$gt: 20
	}
})

// Less than operator
db.users.find({
	age: {
		$lte: 82
	}
})

// Regex operator
db.users.find({
	firstName: {
		$regex: 's',
		$options: 'I'
	}
})

db.users.find({
	firstName: {
		$regex: 'T'
	}
})


// Combining operators
db.users.fin({
	age: {
		$gt: 70
	},
	lastName: {
		$regex: 'g'
	}
})

db.users.find({
	age: {
		$lte: 76
	},
	firstName {
		$regex: 'j',
		$options: 'i'
	}
})


// Field Projection
db.users.find({}, {
	"_id": 0
})

db.users.find({}, {
	"firsName": 1
})

db.users.find({}, {
	"_id": 0
	"firsName": 1
})